# Keycash Backend Code Challenge

O desafio consiste em construir uma API REST (CRUD) de imóveis para keycash.

## O que vamos avaliar?

Tente implementar o máximo que conseguir do teste no tempo determinado. Se não conseguir implementar todas as features sugeridas, fique tranquilo, avaliaremos sua solução pelo que foi entregue :)

Levaremos em conta:

* Organização
* Manutenibilidade
* Rastreabilidade
* Entendimento do problema (sua interpretação, bem como seu entendimento a respeito das regras propostas também faz parte do teste, pois estamos avaliando a forma como você entende e resolve problemas)

Esperamos um projeto **PRODUCTION READY**, logo utilize todas as boas práticas a que está habituado para implementar seu código.

## O que precisa ser implementado?

## API deve ser construída em Node.js

### Utilizamos a seguinte stack: Node.js, Express, TypeORM e MySql (logo será um diferencial se o desafio for entregue nessa stack)

###  API REST/CRUD
  1. Registro de imóvel;
  2. Atualização de um imóvel;
  3. Consulta de um imóvel;
  4. Exclusão de um imóvel;
  5. Filtro de imóveis:
     O filtro consiste em realizar uma consulta de imóveis por número de quartos e/ou metragem e/ou número de vagas.

Seja criativo na escolha dos atributos que considerar necessários para um imóvel a venda. 

### Documentação
  1. Documentar os endpoints da API com Swagger;

### Dicas para o código
* Fique livre para utilizar bibliotecas que possam facilitar seu trabalho; 

## Prazo e como entregar
O prazo para a entrega do desafio são **3 dias** a partir da data que você o recebeu.

Para entregá-lo crie um repositório no GitHub ou Bitbcuket. Para facilitar a identificação do seu desafio, nomeie seu repositório para keycash-backend-challenge-{seu-nome} (ex: keycash-backend-challenge-john-doe).

* Pedimos que você faça um README com pelo menos instruções básicas de como rodar localmente e inclua a documentação na raiz do projeto.

Bom teste!
